# Setup

Open your command line tool within this git repo - after you cloned it - and type `docker-compose up -d` (or with the `-d` flag if you want to see what is happening).
Once the system is up and running you can head over to localhost:8100 and click around.
Enjoy!

# Things to add

- How do I write proper code (Yannik)
- All things have to be a bit better formulated
- more content needs to be added