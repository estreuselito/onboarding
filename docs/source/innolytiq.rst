#########
innolytiq
#########

The Cognaize community developed a plattform named innolytiq. This platform is
the important if you want to start developing your own models as it is the
bridge between the data and the machine learning ready Jupyter Labs. At first
sight it can look quite overwhelming, but you will get used to it and you will
get to know what buttons to press to get to your goal. In order to ease your
introduction into that tool we compiled 
:download:`here <_static/innolytiq_getting_started.pdf>` a started guide.