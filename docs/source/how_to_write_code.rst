#################
How to write code
#################

Hi there. This site gives you a quick overview over what to pay attention to
when you start writing code and developing.

Code
####

When ever you start coding, bear in mind that others should be able to understand
what you wrote. Hence it is important to **always** add at least a docstring
to your functions. Hereafter is a example:

.. code-block:: python

    def complicated_function(a: int, b: str, c: dict, d: list) -> int:
        """This is a really complicated function. It does wild things with the
        provided input and returns an integer which serves as an indicator for
        the wildness of this function. ... (and so on)

        Parameters
        ----------
        a : int
            What does "a" do and where does it come from?
        b : str
            What is "b"? Are there different options which "b" can take?
        c : dict
            Which form has "c"? How should it ideally look like?
        d : list
            More information about "d"

        Returns
        -------
        int
            An indicator of the wildness of this function
        """
        return a

This is the minimal way of documenting your code. Please also try to comment each
line of code, if it is not clear what it does, so that others can directly "read"
and understand your code.