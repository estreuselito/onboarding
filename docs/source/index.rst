.. Onboarding documentation master file, created by
   sphinx-quickstart on Mon Nov  8 13:47:10 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Cognaize!
======================================

Hi there and a warm welcome to **Cognaize**! We are happy to have you here with us!

This website will give you a starting point in your onboarding process here. It
will provide you with information like whom to ask what, what question to ask in
the beginning and much more. Hereafter, you can find a table of contents, which
should serve as orientation - you can just click around and always come back
here.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   who_to_ask
   innolytiq
   README
   how_to_write_code
   corporate_branding

