################
Whom to ask what
################

You will probably have a lot of questions in the beginning. Hence, here is a
list of the people which are important to get to know in the first weeks and
also who can assist you with any questions you might have.

@Ben bitte die Liste ergänzen. Sie liegt unter docs/source/_static/who_to_ask.csv

.. csv-table:: **Whom to ask what**
    :file: _static/who_to_ask.csv
    :widths: 200, 200